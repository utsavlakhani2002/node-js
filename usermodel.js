const mongoose = require('mongoose');

var UsersSchema = new mongoose.Schema({
    
    name: {type:String},
    image: {type:String},
    email: {type:String},
    password: {type:String},
    phone: {type:Number},
    oldimage:{type:String}
});

var users = mongoose.model("user", UsersSchema);
module.exports = users;