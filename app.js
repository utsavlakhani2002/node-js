const http = require("http");
const hostname = "localhost";
const port = 5000;
const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require("body-parser");
bodyParser.json()
const fileUpload = require("express-fileupload");
const mongoose = require('mongoose');
const ObjectID = require('mongodb');
var path = require('path');
app.use(express.json());
require('./db.js')
app.use(express.urlencoded({ extended: true, limit: "50mb" }));
var fs = require("fs");
const { callbackify } = require("util");
app.use(fileUpload());
app.use(function (req, res, next) { global.req = req; next(); });
const user = require('./usermodel')

app.post('/create', (req, res, next) => {

  // console.log(req.files)
  let image = req.files.image;

  var new_user = new user({
    name: req.body.name,
    image: image.name,
    email: req.body.email,
    password: req.body.password,
    phone: req.body.phone,
    _enabled: false
  });
  image.mv('./uploads/' + image.name);

  if (!new_user) {
    return res.json({ "status": "400", "message": "Please enter all fields" })
  } else {

    var insert = new_user.save(function (err) {
      if (!err) {
        console.log("insert successfull");
        return res.json({ "status": "200", "message": "insert successfull" })
      } else {
        console.log(err);
        return res.json({ "status": "400", "message": "data not inserted" })
      }
    });
  }
  return;
});

app.get('/list', (req, res) => {
  console.log(req.body);

  user.find({}, function (err, users) {
    if (!err) {
      console.warn(users);
      return res.json({ "status": "200", "message": "fetch successfull", "data": users })
    } else {
      console.warn(err);
      return res.json({ "status": "400", "message": "data not fetched" })
    }
  });
  return;
});

app.delete("/delete", (req, res) => {
  console.log({ _id: req.body._id });
  user.findByIdAndRemove({ _id: req.body._id }, function (err, users) {
    // console.warn(users);
    if (!err) {
      console.log("Successful delete");
      return res.json({ "status": "200", "message": "Successful delete" })
    } else {
      console.warn(err);
      return res.json({ "status": "400", "message": "data not deleted" })
    }
  });
  return;
});

app.post("/display", (req, res) => {
  console.log({ _id: req.body._id });
  user.find({ _id: req.body._id }, function (err, users) {
    console.warn(users);
    if (!err) {
      console.log("Successful update");
      return res.json({ "status": "200", "message": "Successful fetch", "data": users })
    } else {
      console.warn(err);
      return res.json({ "status": "400", "message": "data not fetch" })
    }
  });
  return;
});

app.post("/update", (req, res) => {
  console.log({ _id: req.body._id });
  let image;
  if (req.files.image) {
    oldimage = req.files.oldimage
    // console.log(oldimage);
    fs.unlinkSync('./uploads/' + oldimage.name)
    image = req.files.image;
  } else {
    image = req.files.oldimage;
  }

  user.findByIdAndUpdate({ _id: req.body._id }, {
    name: req.body.name,
    image: image.name,
    email: req.body.email,
    password: req.body.password,
    phone: req.body.phone,
    _enabled: false
  }, function (err, users) {
    // console.warn(users);
    if (!err) {
      console.log("update Successful");
      return res.json({ "status": "200", "message": "update Successful" })
    } else {
      console.warn(err);
      return res.json({ "status": "400", "message": "data not updated" })
    }
  });
  image.mv('./uploads/' + image.name);



  return;
});

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`));


